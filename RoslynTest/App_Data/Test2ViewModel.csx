﻿#r "System.ComponentModel.DataAnnotations"


using System;
using System.ComponentModel.DataAnnotations;
using RoslynTest.Controllers;

public class TestViewModel
{
    [Required, StringLength(50), CustomValidation(typeof(TestViewModel), "ValidateName")]
    public string Name { get; set; }

    [Required]
    public int? Age { get; set; }

    public static ValidationResult ValidateName(string name, ValidationContext context)
    {
		var ctl = new ValidationController();

		return ctl.ValidateName(name);
    }
}

return typeof(TestViewModel);