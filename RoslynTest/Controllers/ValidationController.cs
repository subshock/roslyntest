﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace RoslynTest.Controllers
{
    public class ValidationController
    {
        public ValidationResult ValidateName(string name)
        {
            return name != null && name.IndexOf("e", StringComparison.OrdinalIgnoreCase) < 0
                ? new ValidationResult("Name requires a letter E within it")
                : ValidationResult.Success;
        }
    }
}