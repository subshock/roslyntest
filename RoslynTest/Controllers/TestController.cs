﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Threading.Tasks;
using System.Web.Hosting;
using System.Web.Http;
using Microsoft.CodeAnalysis.CSharp.Scripting;
using Microsoft.CodeAnalysis.Scripting;
using Newtonsoft.Json;
using RoslynTest.ViewModels;

namespace RoslynTest.Controllers
{
    [RoutePrefix("api/Test")]
    public class TestController : ApiController
    {
        [HttpPost, Route("Test1")]
        public IHttpActionResult Test1(Test1ViewModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            return Ok(model._Data);
        }

        [HttpPost, Route("Test2")]
        public async Task<IHttpActionResult> Test2(Test2ViewModel model)
        {
            if (model == null || !ModelState.IsValid)
                return BadRequest(ModelState);

            var csx = System.IO.File.ReadAllText(HostingEnvironment.MapPath("~/App_Data/Test2ViewModel.csx"));
            var options = ScriptOptions.Default.WithReferences(Assembly.GetExecutingAssembly());
            var modelType = await CSharpScript.EvaluateAsync<Type>(csx, options);
            var obj = JsonConvert.DeserializeObject(model.DataString, modelType);

            Validate(obj);

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            return Ok(obj);
        }

        public class Test1ViewModel
        {
            [Required]
            // ReSharper disable once InconsistentNaming
            public string _Id { get; set; }

            [Required]
            public TestViewModel _Data { get; set; }
        }

        public class Test2ViewModel
        {
            [Required]
            // ReSharper disable once InconsistentNaming
            public string _Id { get; set; }

            [Required]
            // ReSharper disable once InconsistentNaming
            public object _Data { get; set; }

            public string DataString => _Data?.ToString();
        }
    }
}
