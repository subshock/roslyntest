﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.CodeAnalysis.CSharp.Scripting;
using Newtonsoft.Json;

namespace RoslynTest.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Index(string code)
        {
            string fullCode = string.Format("public class Test {{ {0}{1}{0}}}{0}return typeof(Test);",
                Environment.NewLine, code);

            try
            {
                var type = await CSharpScript.EvaluateAsync<Type>(fullCode);
                var obj = Activator.CreateInstance(type);

                ViewBag.Result = JsonConvert.SerializeObject(obj, Formatting.Indented);

            }
            catch (Exception ex)
            {
                ViewBag.Result = ex.Message;
            }

            return View();
        }
    }
}