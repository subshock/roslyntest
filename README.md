# RoslynTest #

This is a proof of concept project for testing the C# scripting through the Roslyn Compiler-as-a-Service for use with dynamically creating view models that can interact with existing assemblies.

The purpose for this is to test the feasibility of using this in a full-scale web application and to see what the various limitations, both in code and performance are likely to be. 